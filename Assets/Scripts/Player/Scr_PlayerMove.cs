﻿using UnityEngine;
using System.Collections;

public class Scr_PlayerMove : MonoBehaviour {

    public GameObject playerGraphic;
    public float maxSpeed = 10f;

    private Rigidbody rb;
    private Vector3 prevLoc;

    public bool canMove;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

	void Update () {
        if(!canMove)
        {
            return;
        }

        Movement();
        GraphicsRotation();

    }


    void Movement()
    {
        if (Input.GetAxisRaw("Vertical") != 0)
            rb.AddForce(Vector3.forward * 100 * Input.GetAxisRaw("Vertical"));

        if (Input.GetAxisRaw("Horizontal") != 0)
            rb.AddForce(Vector3.right * 100 * Input.GetAxisRaw("Horizontal"));

        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);

    }

    void GraphicsRotation()
    {
        if(rb.velocity.sqrMagnitude > 1)
        {
            Quaternion rotation = Quaternion.LookRotation((transform.position + rb.velocity) - transform.position);
            playerGraphic.transform.rotation = Quaternion.Slerp(playerGraphic.transform.rotation, rotation, 0.5f);
        }
        else
        {
            transform.rotation = Quaternion.Euler(Vector3.zero);
        }        
    }


}
