﻿using UnityEngine;
using System.Collections;

public class Scr_ActivateText : MonoBehaviour {

    public TextAsset theText;

    public int startLine;
    public int endLine;

    public Scr_TextboxManager theTextbox;

    public bool requireButtonPress;
    private bool waitForPress;
    public bool destroyWhenActivated;

	void Start () {
        theTextbox = FindObjectOfType<Scr_TextboxManager>();

	}

	void Update () {


        if (!theTextbox.isActive)
            if (waitForPress && Input.GetKeyDown(KeyCode.Space) && theTextbox.waiter == 0)
        {
            theTextbox.ReloadScript(theText);
            theTextbox.currentLine = startLine;
            theTextbox.endAtLine = endLine;
            theTextbox.EnableTextbox();

            if (destroyWhenActivated)
                Destroy(gameObject);
        }
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            if(requireButtonPress)
            {
                waitForPress = true;
                return;
            }

            theTextbox.ReloadScript(theText);
            theTextbox.currentLine = startLine;
            theTextbox.endAtLine = endLine;
            theTextbox.EnableTextbox();

            if (destroyWhenActivated)
                Destroy(gameObject);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.name == "Player")
        {
            waitForPress = false;
        }

    }
}
