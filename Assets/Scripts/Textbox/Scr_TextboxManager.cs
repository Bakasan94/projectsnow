﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Scr_TextboxManager : MonoBehaviour {

    public GameObject textBox;

    public Text theText;
    public TextAsset textfile;
    public string[] textLines;

    public int currentLine;
    public int endAtLine;
    public int waiter = 0;

    public Scr_PlayerMove player;

    public bool isActive;
    public bool stopPlayerMovement;

    private bool isTyping = false;
    private bool cancelTyping = false;

    public float typeSpeed;

    public AudioClip typeSound;
    AudioSource audio;
    void Start()
    {

        player = FindObjectOfType<Scr_PlayerMove>();
        audio = GetComponent<AudioSource>();

        if (textfile != null)
        {
            textLines = (textfile.text.Split('\n'));
        }

        if (isActive)
            EnableTextbox();
        else
            DisableTextbox();


    }

    void Update()
    {
        if (waiter > 0) waiter -= 1;

        if (!isActive)
        {
            return;
        }

        if (endAtLine == 0)
        {
            endAtLine = textLines.Length -1;
        }


        //theText.text = textLines[currentLine];

        if(Input.GetKeyDown(KeyCode.Space) && waiter == 0)
        {
            if (!isTyping)
            {

                currentLine += 1;
                waiter = 1;
                if (currentLine > endAtLine)
                {
                    DisableTextbox();
                }
                else
                {
                    StartCoroutine(TextScroll(textLines[currentLine]));
                }
            }
            else if(isTyping && !cancelTyping)
            {
                cancelTyping = true;
            }
            if (currentLine > endAtLine)
                DisableTextbox();
        }
    }

    public void EnableTextbox()
    {
            waiter = 2;
            textBox.SetActive(true);
            isActive = true;
            if (stopPlayerMovement)
            {
                player.canMove = false;
            }
            StartCoroutine(TextScroll(textLines[currentLine]));
    }

    private IEnumerator TextScroll (string lineOfText)
    {
        int letter = 0;
        theText.text = "";
        isTyping = true;
        cancelTyping = false;
        while(isTyping && !cancelTyping && (letter < lineOfText.Length - 1))
        {
            theText.text += lineOfText[letter];
            letter += 1;
            audio.PlayOneShot(typeSound, 1f);
            yield return new WaitForSeconds(typeSpeed);
            theText.text += lineOfText[letter];
            letter += 1;
            yield return new WaitForSeconds(typeSpeed);
        }
        theText.text = lineOfText;
        isTyping = false;
        cancelTyping = false;
    }


    public void DisableTextbox()
    {
        textBox.SetActive(false);
        isActive = false;
        player.canMove = true;
    }

    public void ReloadScript(TextAsset theText)
    {
        if(theText != null)
        {
            textLines = new string[1];
            textLines = (theText.text.Split('\n'));
        }

    }
}
